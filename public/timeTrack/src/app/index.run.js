(function() {
  'use strict';

  angular
    .module('timeTrack')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
