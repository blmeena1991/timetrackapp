/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('timeTrack')
    .constant('moment', moment);

})();
