/**
 * Created by root on 08/07/17.
 */

(function() {
    'use strict';

    angular
        .module('timeTrack')
        .factory('NetworkFactory', NetworkFactory);

    /** @ngInject */
    function NetworkFactory($http, $q) {

        var factory = {};

        factory.getAccount = function(){
            var defer = $q.defer();
            $http.get('accountInfo')
                .success(function(res){
                    defer.resolve(res);
                })
                .error(function (err, status) {
                    defer.resolve(err);
                });
            return defer.promise;
        }

        factory.getUserInfo = function(){
            var defer = $q.defer();
            $http.get('getUserInfo')
                .success(function(res){
                    defer.resolve(res);
                })
                .error(function (err, status) {
                    defer.resolve(err);
                });
            return defer.promise;
        }

        factory.getTaskList = function(){
            var defer = $q.defer();
            $http.get('taskList')
                .success(function(res){
                    defer.resolve(res);
                })
                .error(function (err, status) {
                    defer.resolve(err);
                });
            return defer.promise;
        }

        factory.getAllTaskList = function(){
            var defer = $q.defer();
            $http.get('adminTask')
                .success(function(res){
                    defer.resolve(res);
                })
                .error(function (err, status) {
                    defer.resolve(err);
                });
            return defer.promise;
        }


        factory.addTimeTask = function(newTimeLog){
            var defer = $q.defer();

            var url="/addTimeTask";
            $http({
                method: 'POST',
                url: url,
                data: newTimeLog
            })
                .success(function(res){
                    defer.resolve(res);
                })
                .error(function(err,status){
                    defer.resolve(err);
                })
            return defer.promise;
        };

        return factory;
    }

})();
