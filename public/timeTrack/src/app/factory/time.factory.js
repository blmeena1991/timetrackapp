/**
 * Created by root on 09/07/17.
 */


/**
 * Created by root on 08/07/17.
 */

(function() {
    'use strict';

    angular
        .module('timeTrack')
        .factory('TimeFactory', TimeFactory);

    /** @ngInject */
    function TimeFactory($http, $q) {

        var factory = {};


        factory.getTimeDiff=function (start, end) {
            var diff = moment(end).diff(moment(start));
            var duration = moment.duration(diff);

            return {
                duration: duration
            }
        }

        factory.getTotalTime=function (timeList) {
            var totalMilliseconds = 0;
            console.log(timeList);
            angular.forEach(timeList, function(key) {
                console.log(key);
                totalMilliseconds += key.loggedTime.duration._milliseconds;
            });

            // After 24 hours, the Moment.js duration object
            // reports the next unit up, which is days.
            // Using the asHours method and rounding down with
            // Math.floor instead gives us the total hours
            return {
                hours: Math.floor(moment.duration(totalMilliseconds).asHours()),
                minutes: moment.duration(totalMilliseconds).minutes()
            }
        }

        return factory;
    }

})();
