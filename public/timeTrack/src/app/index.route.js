(function() {
  'use strict';

  angular
    .module('timeTrack')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        views: {
            'content@': {
                templateUrl: 'timeTrack/src/app/main/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            }
        }
      }).state('admin', {
        url: '/admin',
        views: {
            'content@': {
                templateUrl: 'timeTrack/src/app/admin/admin.html',
                controller: 'AdminController',
                controllerAs: 'admin'
            }
        }
    });

    $urlRouterProvider.otherwise('/');
  }

})();
