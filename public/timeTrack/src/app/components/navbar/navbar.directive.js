(function() {
  'use strict';

  angular
    .module('timeTrack')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'timeTrack/src/app/components/navbar/navbar.html',
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'navbar',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(moment,NetworkFactory) {
      var vm = this;
      vm.islogin=false;
      vm.userInfo=[];
      // "vm.creationDate" is available by directive option "bindToController: true"
      //vm.relativeDate = moment(vm.creationDate).fromNow();

      vm.getAccountInfo = function () {
        NetworkFactory.getAccount().then(function (getAccountInfo) {
            console.log(getAccountInfo);
          if(getAccountInfo && getAccountInfo.user == true ){
            vm.islogin=true;
             vm.getUserInfo();
          }
        })
      };

      vm.getUserInfo = function () {
        NetworkFactory.getUserInfo().then(function (UserInfo) {
            console.log(UserInfo);
          if(UserInfo  ){
            vm.userInfo=UserInfo;
          }
        })
      };


      var init = (function () {
        vm.getAccountInfo();

    })();
    }
  }

})();
