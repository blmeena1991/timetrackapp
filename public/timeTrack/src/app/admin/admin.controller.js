/**
 * Created by root on 08/07/17.
 */

(function() {
    'use strict';

    angular
        .module('timeTrack')
        .controller('AdminController', AdminController);

    /** @ngInject */
    function AdminController($timeout, webDevTec, toastr,NetworkFactory,TimeFactory) {
        var vm = this;
        vm.TaskList=[];
        vm.getAllTaskList = function () {
            NetworkFactory.getAllTaskList().then(function (TaskList) {
                console.log(TaskList);
                if(TaskList){
                    vm.TaskList=TaskList;

                    angular.forEach(vm.TaskList, function(result) {
                        result.loggedTime = TimeFactory.getTimeDiff(result.start_time, result.finish_time);

                    });

                }else{
                    vm.TaskList=[];
                }
            })
        };
        var init = (function () {
            vm.getAllTaskList();

        })();
    }
})();
