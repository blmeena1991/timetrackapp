(function() {
  'use strict';

  angular
    .module('timeTrack')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout,toastr,NetworkFactory,TimeFactory) {
      var vm = this;
      vm.TaskList=[];
      vm.totalTime = {};
      vm.clockIn = new Date();
      vm.clockOut = new Date();
      vm.id='';

      vm.getTaskList = function () {
          NetworkFactory.getTaskList().then(function (TaskList) {
            console.log(TaskList);
              var today = moment(new Date()).format("YYYY-MM-DD");
            if(TaskList){
                vm.TaskList=TaskList;

                angular.forEach(vm.TaskList, function(result) {

                    // Add the loggedTime property which calls
                    // getTimeDiff to give us a duration object
                    result.loggedTime = TimeFactory.getTimeDiff(result.start_time, result.finish_time);



                    var c =  moment(result.created).format("YYYY-MM-DD");
                    console.log(c);
                    result.isedit=false;
                    if(c==today) {
                        result.isedit=true;
                    }


                });

                vm.updateTotalTime(vm.TaskList);
            }else{
                vm.TaskList=[];
            }
          })
    };

    vm.updateTask=function (TaskList) {
        vm.id= TaskList.id;
        vm.title= TaskList.title;
        vm.description= TaskList.description;
        var startHH =moment(TaskList.start_time).format("HH");
        var startmm =moment(TaskList.start_time).format("mm");
        var startd = new Date();
        startd.setHours( startHH );
        startd.setMinutes( startmm );
        vm.clockIn = startd;

        var endHH =moment(TaskList.finish_time).format("HH");
        var endmm =moment(TaskList.finish_time).format("mm");
        var endd = new Date();
        endd.setHours( endHH );
        endd.setMinutes( endmm );
        vm.clockOut = endd;
        console.log(TaskList);
    }


      vm.updateTotalTime=function (TaskList) {
      vm.totalTime = TimeFactory.getTotalTime(TaskList);
        console.log(vm.totalTime);
    }


    vm.addTimeTask = function() {
      // Make sure that the clock-in time isn't after
      // the clock-out time!
      if(vm.clockOut < vm.clockIn) {
          alert("You can't clock out before you clock in!");
          return;
      }
      if(vm.clockOut - vm.clockIn === 0) {
          alert("Your time entry has to be greater than zero!");
          return;
      }

        var newTimeLog={
          "start_time":moment(vm.clockIn).format("YYYY-MM-DD HH:mm:ss"),
          "finish_time":moment(vm.clockOut).format("YYYY-MM-DD HH:mm:ss"),
          "title":vm.title ,
          "description":vm.description,
           "id":vm.id
        };
        console.log(newTimeLog);
        NetworkFactory.addTimeTask(newTimeLog).then(function (response) {
            if (response && response.success == true) {
                toastr.success('New Time Task  log successfully');
            }else{
                toastr.error('Error.Please contact to Admin Team.');
            }
        })
        vm.description = ""
        vm.title = "";
        vm.getTaskList();
    }


    var init = (function () {
      vm.getTaskList();

    })();
  }
})();
