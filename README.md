# TimeTrack APP


## Install Instructions


1. Clone the repo: `git clone https://bitbucket.org/blmeena1991/timetrackapp.git`
2. Install packages: `npm install`
3. Change out the database configuration in configuration/config.js
4. Create time_app database in local machine and import the database  schema/time_app.sql
5. Launch: `node server.js`
6. Visit in your browser at: `http://localhost:3000`

