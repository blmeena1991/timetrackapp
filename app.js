var express           =     require('express')
  , passport          =     require('passport')
  , util              =     require('util')
  , FacebookStrategy  =     require('passport-facebook').Strategy
  , session           =     require('express-session')
  , cookieParser      =     require('cookie-parser')
  , bodyParser        =     require('body-parser')
  , config            =     require('./configuration/config')
  , mysql             =     require('mysql')
  , app               =     express();

var apiResponse = require('express-api-response');
var SqlString = require('sqlstring');
apiResponse.options({
    emptyArrayIsOk: true
});

var path = require('path');
//Define MySQL parameter in Config.js file.
var connection = mysql.createConnection({
  host     : config.host,
  user     : config.username,
  password : config.password,
  database : config.database
});

//Connect to Database only if Config.js parameter is set.

if(config.use_database==='true')
{
    connection.connect();
}

// Passport session setup.
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});


// Use the FacebookStrategy within Passport.

passport.use(new FacebookStrategy({
    clientID: config.facebook_api_key,
    clientSecret:config.facebook_api_secret ,
    callbackURL: config.callback_url
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function () {
      //Check whether the User exists or not using profile.id
      if(config.use_database==='true')
      {
      connection.query("SELECT * from user where user_id="+profile.id,function(err,rows,fields){
        if(err) throw err;
        if(rows.length===0)
          {
            console.log("There is no such user, adding now");
            connection.query("INSERT into user(user_id,user_name) VALUES('"+profile.id+"','"+profile.displayName+"')");
          }
          else
            {
              console.log("User already exists in database");
            }
          });
      }
      return done(null, profile);
    });
  }
));


app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');



app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ secret: 'keyboard cat', key: 'sid'}));
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, 'public')));

//app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
  res.render('index', { user: req.user });
});


app.get('/home',ensureAuthenticated, function(req, res){
  res.render('home.ejs');
});

app.post('/addTimeTask',ensureAuthenticated, function(req, res){
    var obj = req.body;
    console.log(req.body);
     var result={success:false};
     console.log(obj.length);
    if(obj)
    {
        if(obj.id){

            connection.query("UPDATE task SET title= "+SqlString.escape(obj.title)+",description="+SqlString.escape(obj.description)+",start_time='"+obj.start_time+"',finish_time='"+obj.finish_time+"' where id="+obj.id);

        }else{

            connection.query("INSERT into task(user_id,title,description,start_time,finish_time) VALUES('"+req.user.id+"',"+SqlString.escape(obj.title)+","+SqlString.escape(obj.description)+",'"+obj.start_time+"','"+obj.finish_time+"')");

        }

        result.success=true;
    }else{
        result.success=false;
    }
    res.end(JSON.stringify(result));
});

app.get('/adminTask', function(req, res, next){
    if ( req.isAuthenticated()) {
        connection.query("select task.*,user.user_id,user.user_name from task left join user on user.user_id=task.user_id", function (err, rows, fields) {
            if (err) throw err;
            console.log(rows.length);
            if (rows.length === 0) {
                res.end(JSON.stringify({}));
            } else {
                res.end(JSON.stringify(rows));
            }
        });
    }
});

app.get('/getUserInfo',ensureAuthenticated, function(req, res, next){
    res.data = { user: req.user};
    next();
}, apiResponse);

app.get('/accountInfo', function(req, res, next){
    console.log(req.user);
    res.data = { user: req.isAuthenticated()};
    next();
}, apiResponse);


app.get('/taskList', function(req, res, next){
    if (req.user != undefined ) {

        connection.query("select task.*,user.user_id,user.user_name from task left join user on user.user_id=task.user_id  where task.user_id=" + req.user.id, function (err, rows, fields) {
            if (err) throw err;
            console.log(rows.length);
            if (rows.length === 0) {
                res.end(JSON.stringify({}));
            } else {
                res.end(JSON.stringify(rows));
            }
        });
    }
});


app.get('/auth/facebook', passport.authenticate('facebook',{scope:'email'}));


app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { successRedirect : '/home', failureRedirect: '/' }),
  function(req, res) {
    res.redirect('/');
  });

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});




function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/')
}

app.listen(3000);
